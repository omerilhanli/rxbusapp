package com.ilhanli.omer.rxbussample;

import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxBus.unregister(this);
    }


}
