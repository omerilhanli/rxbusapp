package com.ilhanli.omer.rxbussample;

import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxBus.unregister(this);
    }

}
