package com.ilhanli.omer.rxbussample;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toast;

import com.ilhanli.omer.rxbussample.model.Event;
import com.ilhanli.omer.rxbussample.model.EventBus;

import java.util.function.Consumer;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.container, new MyFragment()).commit();

        RxBus.subscribe(RxBus.SUBJECT_MY_SUBJECT, this, event -> {

            if (event instanceof EventBus) {

                EventBus eventBus = ((EventBus) event);

                Toast.makeText(MainActivity.this, "event:" + eventBus.msg, Toast.LENGTH_SHORT).show();

            }
        });
    }
}
