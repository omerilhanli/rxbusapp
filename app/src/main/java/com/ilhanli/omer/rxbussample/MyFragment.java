package com.ilhanli.omer.rxbussample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ilhanli.omer.rxbussample.model.EventBus;

/*
https://piercezaifman.com/how-to-make-an-event-bus-with-rxjava-and-rxandroid/
 */

public class MyFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment, container, false);

        //Using Retrolambda
        v.findViewById(R.id.btn).setOnClickListener(view -> {

            RxBus.publish(RxBus.SUBJECT_MY_SUBJECT, new EventBus("HellfoFromFragment"));

        });

        return v;
    }

}
