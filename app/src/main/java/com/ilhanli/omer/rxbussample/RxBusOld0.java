package com.ilhanli.omer.rxbussample;

import com.ilhanli.omer.rxbussample.model.Event;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

public final class RxBusOld0 {

    private static PublishSubject<Event> sSubject = PublishSubject.create();

    private RxBusOld0() {
        // hidden constructor
    }

    public static Disposable subscribe(@NonNull Consumer<Event> action) {
        return sSubject.subscribe(action);
    }

    public static void publish(@NonNull Event message) {
        sSubject.onNext(message);
    }
}