package com.ilhanli.omer.rxbussample.model;

import com.ilhanli.omer.rxbussample.model.Event;

public class EventBus extends Event {

    public EventBus(String msg)
    {
        this.msg=msg;
    }

    public String msg;

}
